provider "aws" {
  region = "${var.region}"
}

terraform {
  required_version = ">= 0.11.13"
}

module "terraform_state_backend" {
  source     = "git::https://github.com/cloudposse/terraform-aws-tfstate-backend.git?ref=master"
  namespace  = "${var.namespace}"
  stage      = "${var.stage}"
  name       = "${var.name}"
  attributes = ["state"]
  region     = "${var.region}"
  tags       = "${var.tags}"
}
