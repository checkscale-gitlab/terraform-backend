SHELL = /bin/sh

export ROOTS_VERSION=master

export COMPANY_NAME=SkaleSys
export COMPANY_WEBSITE=https://skalesys.com

export LOGO_THEME=nature,forest

#export COMMERCIAL=
-include $(shell [ ! -d .roots ] && git clone --branch $(ROOTS_VERSION) https://gitlab.com/skalesys/roots.git .roots; echo .roots/Makefile)


.PHONY: install
## Install the project requirements
install:
	@make --silent terraform/install
	@make --silent gomplate/install
