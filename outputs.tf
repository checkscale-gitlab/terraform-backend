output "dynamodb_table_arn" {
  description = "DynamoDB table ARN"
  value       = "${module.terraform_state_backend.dynamodb_table_arn}"
}

output "dynamodb_table_id" {
  description = "DynamoDB table ID"
  value       = "${module.terraform_state_backend.dynamodb_table_id}"
}

output "dynamodb_table_name" {
  description = "DynamoDB table name"
  value       = "${module.terraform_state_backend.dynamodb_table_name}"
}

output "s3_bucket_arn" {
  description = "S3 bucket ARN"
  value       = "${module.terraform_state_backend.s3_bucket_arn}"
}

output "s3_bucket_domain_name" {
  description = "S3 bucket domain name"
  value       = "${module.terraform_state_backend.s3_bucket_domain_name}"
}

output "s3_bucket_id" {
  description = "S3 bucket ID"
  value       = "${module.terraform_state_backend.s3_bucket_id}"
}

output "terraform_backend_config" {
  description = "Rendered Terraform backend config file"
  value       = "${module.terraform_state_backend.terraform_backend_config}"
}
